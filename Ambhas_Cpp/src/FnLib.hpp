

#ifndef FnLib_H
#define FnLib_H

#include <string>
#include <map>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

namespace Ambhas {

    const double no_data_value = 999.;// valeur par defaut lorsque la grille et vide

    void readGrd(Eigen::MatrixXd& m, std::map<string,double>& hash, string grdfile) ;

     /*
    Input:
         nrow:       number of rows of the watershed
         ncols:        number of cols of the watershed
        watershed:  map of watershed; 1 means inside and 0 means outside watershed
        hini:       initial groundwater level
        T:          transmissivity as a matrix
        Sy:         specific yield
        dx:         cell size (in m for square shape)
        dt:         time step in seconds
        hmin:       groundwater level corresponding to zero discharge
        par_discharge: parameter controlling the discharge (range is 0 to 1) 
        net_recharge:   net recharge to the groundwater (recharge-groundwater pumping)
        alpha:      parameter controlling resolution algo (1.0 ~ implicit) 
    
    Output:
        hnew:       groundwater level at next time step
        discharge:  baseflow 
        max_neumann_criterion: value of the maximum neumann number
     */
    void ambhas_gw_2d_xy(
            int nrows,
            int ncols,
            Eigen::MatrixXd& watershed, 
            Eigen::MatrixXd& hini, 
            const Eigen::MatrixXd& T, 
            const Eigen::MatrixXd& hmin, 
            const Eigen::MatrixXd& par_discharge, 
            const Eigen::MatrixXd& net_recharge, 
            const Eigen::MatrixXd& Sy, 
            const double dx, 
            const double dt,
            const double alpha,
            Eigen::MatrixXd& hnew,
            Eigen::MatrixXd& discharge,
            double& max_neumann_criterion) ;


//API utilisant les types double** à la place de Eigen::MatrixXd
    void readGrd(double**& m, std::map<string,double>& hash, string grdfile) ;

    void ambhas_gw_2d_xy(
            const int nrows,
            const int ncols,
            double**& watershed, 
            double**& hini, 
            double**& T, 
            double**& hmin, 
            double**& par_discharge, 
            double**& net_recharge, 
            double**& Sy, 
            const double dx, 
            const double dt,
            const double alpha,
            double**& hnew,
            double**& discharge,
            double& max_neumann_criterion) ;
            
    double min0(double x);
}
#endif
