/**
 * @file src/Ambhas_cpp.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2017-2018 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time, ext.Eigen @@endtagdepends

#include <iomanip>      // std::setprecision
#include <iostream>     // std::cout
#include <limits>       // std::numeric_limits<T>::infinity

#include <vle/DiscreteTime.hpp>

#include <vle/utils/Package.hpp>
#include <FnLib.hpp>// le fichier header des fonctions readGrd et ambhas_gw_2d_xy

namespace vd = vle::devs;
namespace vv = vle::value;

namespace vu = vle::utils;

namespace Ambhas {
using namespace vle::discrete_time;

class Ambhas_cpp : public DiscreteTimeDyn
{
public:

    Ambhas_cpp(
        const vd::DynamicsInit& init,
        const vd::InitEventList& events)
        : DiscreteTimeDyn(init, events)
    {
        DbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.;
        
        if (DbgLog > 0.) std::cout << "\t[Ambhas_cpp]\tStarting constructor" << std::endl;

        getVPZParameters(events);
       
        init_grd();//watershed, nrows & ncols values read, allocate memory for watershed and dem(opt)
        
        addVariables(events);// Allocate memory for hini, mRecharge
        
        initValue(0.);

        if (DbgLog > 0.) std::cout << "\t[Ambhas_cpp]\tEnding constructor" << std::endl;
    };


    virtual ~Ambhas_cpp() 
    {};

    virtual void finish()
    {
        if (DbgLog > 0.) std::cout << "\t[Ambhas_cpp]\tMemory cleaning start" << std::endl;
        free_Carrayptrs(watershed);
        free_Carrayptrs(hini);
        free_Carrayptrs(mRecharge);
        free_Carrayptrs(dem);
        free_Carrayptrs(sy);
        free_Carrayptrs(hmin);
        free_Carrayptrs(par_discharge);
        free_Carrayptrs(mT);
        if (DbgLog > 0.) std::cout << "\t[Ambhas_cpp]\tMemory cleaning end" << std::endl;
    }


    void compute(const vle::devs::Time& time)
    {
        if (DbgLog > 0.) std::cout << std::setprecision(10) << time << "\t[Ambhas_cpp]\tStarting compute" << std::endl;

        compute_actual_net_recharge(&actual_net_recharge, &h1, &potential_net_recharge, &sy, ncols, nrows, DbgLog, time);//change values of actual_net_recharge
        
        convert_recharge(&mRecharge, &actual_net_recharge, &watershed, ncols, nrows, n, time); //change values of mRecharge

        compute_one_step(time); // compute and update Vars
        
        if (DbgLog > 0.) std::cout << std::setprecision(10) << time << "\t[Ambhas_cpp]\tEnding compute" << std::endl;
    };

    void initValue(const vd::Time& time)
    {
        if (DbgLog > 0.) std::cout << std::setprecision(10) << time << "\t[Ambhas_cpp]\tStarting initValue" << std::endl;

        unsigned int counter = 0;
        for (int i = 0; i < nrows; ++i) {
            for (int j = 0; j < ncols; ++j) {
                if (watershed[i][j] > 0.) {
                    h1[counter] = hini[i][j];
                    ++counter;
                }
            }
        }

        if (DbgLog > 0.) std::cout << std::setprecision(10) << time << "\t[Ambhas_cpp]\tEnding initValue" << std::endl;
    };



private:
    std::vector<Var> potential_net_recharge;
    std::vector<Var> h1;
    std::vector<Var> actual_net_recharge;
    std::vector<Var> discharge;
    std::vector<Var> gwvol;
    std::vector<Var> gw_overland_flow;

    double DbgLog;
    std::string mInputGrid;
    std::string mInputGrid_hini;
    std::string mDataPath;
    std::string mDataPath_hini;
    std::string mDataPath_Sy;
    std::string mDataPath_hmin;
    std::string mDataPath_par_discharge;
    std::string mDataPath_T;
    int gw_dt;
    double gw_Sy, gw_par_discharge, gw_T, gw_dem, gw_hmin, gw_hini;
    std::string mNamingMode; //Type de convention pour le nommage des identifiants parcelle (id unique ou couple x_y)
    double mRechargeConversionFactor;
    std::string PackageName;
    std::string mDemPath;
    double mSoilThickness;

    int nrows, ncols, n, nn;
    double dx, mGWpixelArea;
    bool mUseHiniGrd;
    bool mUseDemGrd;
    bool mUseSyGrd;
    bool mUseHminGrd;
    bool mUseDischargeGrd;
    bool mUseTGrd;
    double **watershed; // 0/1 map 
    double **hini;
    double **mRecharge;
    double **dem;
    double **sy;
    double **hmin;
    double **par_discharge;
    double **mT;

    std::map<string,double> param ; // parametres du fichier grd


    //Lecture configuration de l'Executive (paramètres VPZ)
    void getVPZParameters(const vd::InitEventList& events)
    {
        if (DbgLog > 0.)
            std::cout << "\t[Ambhas_cpp]\t\tReading experimental conditions" << std::endl;
        n = (events.exist("n")) ? vv::toInteger(events.get("n")) : 0; //number of active plot in watershed from GenGRDcan to compare to local active plots

        PackageName = (events.exist("PkgName")) ? vv::toString(events.get("PkgName")) : "Ambhas_Cpp";
        mInputGrid = (events.exist("InputGrid")) ? vv::toString(events.get("InputGrid")) : "watershed.grd";
        auto ctx = vu::make_context();
        vu::Package mPack(ctx, PackageName);
        mDataPath = mPack.getDataFile(mInputGrid); //search input file in the pkg data folder

        if (events.exist("DemGrid")) {
            mUseDemGrd=true;
            mDemPath = mPack.getDataFile(vv::toString(events.get("DemGrid"))); //search input file in the pkg data folder
        } else {
            mUseDemGrd=false;
        }
        if (events.exist("InputGrid_hini")) {
            mUseHiniGrd=true;
            mDataPath_hini = mPack.getDataFile(vv::toString(events.get("InputGrid_hini"))); //search input file in the pkg data folder
        } else {
            mUseHiniGrd=false;
        }
        if (events.exist("InputGrid_Sy")) {
            mUseSyGrd=true;
            mDataPath_Sy = mPack.getDataFile(vv::toString(events.get("InputGrid_Sy"))); //search input file in the pkg data folder
        } else {
            mUseSyGrd=false;
        }
        if (events.exist("InputGrid_hmin")) {
            mUseHminGrd=true;
            mDataPath_hmin = mPack.getDataFile(vv::toString(events.get("InputGrid_hmin"))); //search input file in the pkg data folder
        } else {
            mUseHminGrd=false;
        }
        if (events.exist("InputGrid_par_discharge")) {
            mUseDischargeGrd=true;
            mDataPath_par_discharge = mPack.getDataFile(vv::toString(events.get("InputGrid_par_discharge"))); //search input file in the pkg data folder
        } else {
            mUseDischargeGrd=false;
        }
        if (events.exist("InputGrid_T")) {
            mUseTGrd=true;
            mDataPath_T = mPack.getDataFile(vv::toString(events.get("InputGrid_T"))); //search input file in the pkg data folder
        } else {
            mUseTGrd=false;
        }

        gw_dem = (events.exist("dem")) ? vv::toDouble(events.get("dem")) : std::numeric_limits<double>::max(); //same dem used for all cell in the watershed
        gw_dt = (events.exist("dt")) ? vv::toInteger(events.get("dt")) : 1;
        gw_hmin = (events.exist("hmin")) ? vv::toDouble(events.get("hmin")) : 580.; //same hmin used for all cell in the watershed
        gw_hini = (events.exist("hini")) ? vv::toDouble(events.get("hini")) : 600.; //same hini used for all cell in the watershed
        gw_Sy = (events.exist("Sy")) ? vv::toDouble(events.get("Sy")) : 0.005;
        gw_par_discharge = (events.exist("par_discharge")) ? vv::toDouble(events.get("par_discharge")) : 0.999;
        gw_T = (events.exist("T")) ? vv::toDouble(events.get("T")) : 20.;
        mNamingMode = (events.exist("NamingMode")) ? vv::toString(events.get("NamingMode")) : "x_y";
        mRechargeConversionFactor = (events.exist("RechargeConversionFactor")) ? vv::toDouble(events.get("RechargeConversionFactor")) : 1.;
        mSoilThickness = (events.exist("SoilThickness")) ? vv::toDouble(events.get("SoilThickness")) : 0.;

        if (DbgLog > 0.) {
            std::cout << "\t[Ambhas_cpp]\t\t[p]\tPackage Name for data:\t" << PackageName << std::endl;
            std::cout << "\t[Ambhas_cpp]\t\t[p]\twatershed grid input file:\t" << mDataPath << std::endl;
            if (mUseDemGrd) {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tDEM grid input file:\t" << mDemPath << "\t(soil surface elevation in m for all cells)" << std::endl;
            } else {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tdem:\t" << gw_dem << "\t(soil surface elevation in m)" << std::endl;
            }
            std::cout << "\t[Ambhas_cpp]\t\t[p]\tSoilThickness:\t" << mSoilThickness << "\t(m, distance between soil surface and GW max height)" << std::endl;
            if (mUseHiniGrd) {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\thini grid input file:\t" << mDataPath_hini << "\t(initial groundwater level for all cells)" << std::endl;
            } else {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\thini:\t" << gw_hini << "\t(initial groundwater level)" << std::endl;
            }
            std::cout << "\t[Ambhas_cpp]\t\t[p]\tn:\t" << n << "\t(number of expected external model connected, emulate random if 0)" << std::endl;
            std::cout << "\t[Ambhas_cpp]\t\t[p]\tdt:\t" << gw_dt << "\t(time step in seconds)" << std::endl;
            if (mUseHminGrd) {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\thmin grid input file:\t" << mDataPath_hmin << "\t(groundwater level corresponding to zero discharge for all cells)" << std::endl;
            } else {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\thmin:\t" << gw_hmin << "\t(groundwater level corresponding to zero discharge" << std::endl;
            }
            if (mUseSyGrd) {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tSy grid input file:\t" << mDataPath_Sy << "\t(specific yield for all cells)" << std::endl;
            } else {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tSy:\t" << gw_Sy << "\t(specific yield)" << std::endl;
            }
            if (mUseDischargeGrd) {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tpar_discharge grid input file:\t" << mDataPath_par_discharge << "\t(parameter controlling the discharge (range is 0 to 1) for all cells)" << std::endl;
            } else {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tpar_discharge:\t" << gw_par_discharge << "\t(parameter controlling the discharge (range is 0 to 1))" << std::endl;
            }
            if (mUseTGrd) {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tT grid input file:\t" << mDataPath_T << "\t(transmissivity for all cells)" << std::endl;
            } else {
                std::cout << "\t[Ambhas_cpp]\t\t[p]\tT:\t" << gw_T << "\t(transmissivity)" << std::endl;
            }
            std::cout << "\t[Ambhas_cpp]\t\t[p]\tNamingMode:\t" << mNamingMode << "\t(used naming convention)" << std::endl;
        }
    };

    void init_grd()
    {
        if (DbgLog > 0.) std::cout << "\t[Ambhas_cpp]\t\tRunning init function" << std::endl;
            
        //get watershed, dx, nrows & ncols values back
        // allocate memory for watershed
        readGrd(watershed,param,mDataPath);
        nrows = (int)param["nrows"];
        ncols = (int)param["ncols"];
        dx = (int)param["cellsize"];
        mGWpixelArea = std::pow(dx, 2);
        
        if (DbgLog > 0.) {
            std::cout << "\t[Ambhas_cpp]\t\tRead watershed description : " << mDataPath << std::endl;
            std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
            std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
            std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
        }

        if (mUseHiniGrd) {
            // allocate memory for hini (if mUseHiniGrd) else done in addVariables()
            readGrd(hini,param,mDataPath_hini);
            
            if (DbgLog > 0.) {
                std::cout << "\t[Ambhas_cpp]\t\tRead hini grid file : " << mDataPath_hini << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
            }
        }

        if (mUseDemGrd) {
            // allocate memory for dem (if mUseDemGrd) else done in addVariables()
            readGrd(dem,param,mDemPath);
            
            if (DbgLog > 0.) {
                std::cout << "\t[Ambhas_cpp]\t\tRead DEM grid file : " << mDemPath << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
            }
        }

        if (mUseSyGrd) {
            // allocate memory for sy (if mUseSyGrd) else done in addVariables()
            readGrd(sy,param,mDataPath_Sy);
            
            if (DbgLog > 0.) {
                std::cout << "\t[Ambhas_cpp]\t\tRead Sy grid file : " << mDataPath_Sy << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
            }
        }

        if (mUseHminGrd) {
            // allocate memory for sy (if mUseHminGrd) else done in addVariables()
            readGrd(hmin,param,mDataPath_hmin);
            
            if (DbgLog > 0.) {
                std::cout << "\t[Ambhas_cpp]\t\tRead hmin grid file : " << mDataPath_hmin << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
            }
        }

        if (mUseDischargeGrd) {
            // allocate memory for par_discharge (if mUseDischargeGrd) else done in addVariables()
            readGrd(par_discharge,param,mDataPath_par_discharge);
            
            if (DbgLog > 0.) {
                std::cout << "\t[Ambhas_cpp]\t\tRead par_discharge grid file : " << mDataPath_par_discharge << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
            }
        }

        if (mUseTGrd) {
            // allocate memory for mT (if mUseTGrd) else done in addVariables()
            readGrd(mT,param,mDataPath_T);
            
            if (DbgLog > 0.) {
                std::cout << "\t[Ambhas_cpp]\t\tRead T grid file : " << mDataPath_T << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tPixel Area :" << mGWpixelArea  << " (m2)"<< std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound nrows :" << nrows << std::endl;
                std::cout << "\t[Ambhas_cpp]\t\t\tFound ncols :" << ncols << std::endl;
            }
        }
    };
    

    // Allocate memory for hini(if !mUseHiniGrd, else done in init_grd()), mRecharge
    // create Var & Sync
    void addVariables(const vd::InitEventList& events) 
    {
        if (n != 1) {
            unsigned int counter = 0;
            for (int i = 0; i < nrows; ++i) {
                for (int j = 0; j < ncols; ++j) {
                    if (DbgLog > 0.)
                        std::cout << watershed[i][j] << "  ";
                    if (watershed[i][j] > 0.) {
                        std::stringstream ss1, ss2, ss3, ss4, ss5, ss6;
                        if (mNamingMode == "x_y") {
                            ss1 << "actual_net_recharge_" << i << "_" << j;
                            ss2 << "H_" << i << "_" << j;
                            ss3 << "discharge_" << i << "_" << j;
                            ss4 << "potential_net_recharge_" << i << "_" << j;
                            ss5 << "GWVol_" << i << "_" << j;
                            ss6 << "GW_Overland_Flow_" << i << "_" << j;
                        } else {
                            ss1 << "actual_net_recharge_" << counter;
                            ss2 << "H_" << counter;
                            ss3 << "discharge_" << counter;
                            ss4 << "potential_net_recharge_" << counter;
                            ss5 << "GWVol_" << counter;
                            ss6 << "GW_Overland_Flow_" << counter;
                        }
                        actual_net_recharge.push_back(Var());
                        actual_net_recharge[actual_net_recharge.size()-1].init(this, ss1.str(), events);
                        h1.push_back(Var());
                        h1[h1.size()-1].init(this, ss2.str(), events);
                        discharge.push_back(Var());
                        discharge[discharge.size()-1].init(this, ss3.str(), events);
                        gwvol.push_back(Var());
                        gwvol[gwvol.size()-1].init(this, ss5.str(), events);
                        gw_overland_flow.push_back(Var());
                        gw_overland_flow[gw_overland_flow.size()-1].init(this, ss6.str(), events);

                        if (n > 1) {
                            potential_net_recharge.push_back(Var());
                            potential_net_recharge[potential_net_recharge.size()-1].init(this, ss4.str(), events);
                            getOptions().syncs.insert(std::make_pair(ss4.str(), 1)); // force sync
                        }
                        ++counter;
                    }
                }
                if (DbgLog > 0.)
                    std::cout << std::endl;
            }
        } else {
            actual_net_recharge.push_back(Var());
            actual_net_recharge[actual_net_recharge.size()-1].init(this, "actual_net_recharge", events);
            h1.push_back(Var());
            h1[h1.size()-1].init(this, "H", events);
            discharge.push_back(Var());
            discharge[discharge.size()-1].init(this, "discharge", events);
            gwvol.push_back(Var());
            gwvol[gwvol.size()-1].init(this, "GWVol", events);
            gw_overland_flow.push_back(Var());
            gw_overland_flow[gw_overland_flow.size()-1].init(this, "GW_Overland_Flow", events);
            if (n > 0) {
				potential_net_recharge.push_back(Var());
                potential_net_recharge[potential_net_recharge.size()-1].init(this, "potential_net_recharge", events);
                getOptions().syncs.insert(std::make_pair("potential_net_recharge", 1)); // force sync
            }
        }

        if (DbgLog > 1.) std::cout << "\t[Ambhas_cpp]\t\tinitial water levels :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        nn = 0;
        if (!mUseHiniGrd) hini = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            if (!mUseHiniGrd) hini[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
               if (!mUseHiniGrd) hini[i][j] = watershed[i][j] * gw_hini;
               if (DbgLog > 1.)
                    std::cout << hini[i][j] << "  ";
               if (watershed[i][j] > 0.)
                    ++nn;
            }
            if (DbgLog > 1.)
                std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;


        if (DbgLog > 1.) std::cout << "\t[Ambhas_cpp]\t\tDEM elevations :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        nn = 0;
        if (!mUseDemGrd) dem = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            if (!mUseDemGrd) dem[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
               if (!mUseDemGrd) dem[i][j] = watershed[i][j] * gw_dem;
               if (DbgLog > 1.)
                    std::cout << dem[i][j] << "  ";
               if (watershed[i][j] > 0.)
                    ++nn;
            }
            if (DbgLog > 1.)
                std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog > 1.) std::cout << "\t[Ambhas_cpp]\t\tSy :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        nn = 0;
        if (!mUseSyGrd) sy = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            if (!mUseSyGrd) sy[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
               if (!mUseSyGrd) sy[i][j] = watershed[i][j] * gw_Sy;
               if (DbgLog > 1.)
                    std::cout << sy[i][j] << "  ";
               if (watershed[i][j] > 0.)
                    ++nn;
            }
            if (DbgLog > 1.)
                std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog > 1.) std::cout << "\t[Ambhas_cpp]\t\thmin :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        nn = 0;
        if (!mUseHminGrd) hmin = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            if (!mUseHminGrd) hmin[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
               if (!mUseHminGrd) hmin[i][j] = watershed[i][j] * gw_hmin;
               if (DbgLog > 1.)
                    std::cout << hmin[i][j] << "  ";
               if (watershed[i][j] > 0.)
                    ++nn;
            }
            if (DbgLog > 1.)
                std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog > 1.) std::cout << "\t[Ambhas_cpp]\t\tpar_discharge :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        nn = 0;
        if (!mUseDischargeGrd) par_discharge = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            if (!mUseDischargeGrd) par_discharge[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
               if (!mUseDischargeGrd) par_discharge[i][j] = watershed[i][j] * gw_par_discharge;
               if (DbgLog > 1.)
                    std::cout << par_discharge[i][j] << "  ";
               if (watershed[i][j] > 0.)
                    ++nn;
            }
            if (DbgLog > 1.)
                std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog > 1.) std::cout << "\t[Ambhas_cpp]\t\tT :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        nn = 0;
        if (!mUseTGrd) mT = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            if (!mUseTGrd) mT[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
               if (!mUseTGrd) mT[i][j] = watershed[i][j] * gw_T;
               if (DbgLog > 1.)
                    std::cout << mT[i][j] << "  ";
               if (watershed[i][j] > 0.)
                    ++nn;
            }
            if (DbgLog > 1.)
                std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;

        mRecharge = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            mRecharge[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
                mRecharge[i][j] = 0.;
            }
        }

        if (n == 0) {
            if (DbgLog > 0.)
                std::cout << "\t[Ambhas_cpp]\tnet_recharge will be simulated (random)" << std::endl;
        } else if ((n > 0) && (n != nn)) {
            std::stringstream ss;
            ss << "Warning!!! strange same number of active plots. Expect:" << n << "\tFound:" << nn << std::endl;
            throw vu::ModellingError(ss.str());
        }
    };

    void compute_actual_net_recharge(std::vector<Var>* ActualNetRecharge, 
        std::vector<Var>* H, std::vector<Var>* PotentialNetRecharge, 
        double*** Sy, const int& ncols, const int& nrows, const double& DbgLog, const vd::Time& time)
    {
        int count = 0;
        for (int i = 0; i < nrows; ++i) {
            for (int j = 0; j < ncols; ++j) {
                if (watershed[i][j] > 0.) {
                    if ((DbgLog > 0.) && ((*H)[count](-1) + (*PotentialNetRecharge)[count]() * mRechargeConversionFactor / mGWpixelArea / (*Sy)[i][j] < 0.)) {
                        std::cout << std::setprecision(10) << time << "\t[Ambhas_cpp]\t\tpotential net recharge reduced to currently available water" << std::endl;
                        if (DbgLog > 1.) {
                            std::cout<< "\t\tid:" << count << "\t" << mRechargeConversionFactor*(*PotentialNetRecharge)[count]() / mGWpixelArea << " -> " << -(*H)[count](-1)*(*Sy)[i][j] << std::endl;
                        }
                    }
                    (*ActualNetRecharge)[count] = std::max(mRechargeConversionFactor*(*PotentialNetRecharge)[count]() / mGWpixelArea, -(*H)[count](-1)*(*Sy)[i][j]);
                    
                    ++count;
                }
            }
        }
        
    };


     void convert_recharge(double*** RechargePtr, std::vector<Var>* ActualNetRecharge, 
        double*** Watershed, const int& ncols, const int& nrows, const int& n, const vd::Time& time)
    {
        int count = 0;
        for (int i = 0; i < nrows; ++i) {
            for (int j = 0; j < ncols; ++j) {
                if (n == 0) {
                    srand (i+nrows*j+nrows*ncols*(int)time);
                    (*RechargePtr)[i][j] = ((*Watershed)[i][j] > 0.) ? ((double)rand() / RAND_MAX )*0.04-0.02 : 0.;
                    (*ActualNetRecharge)[count] = (*RechargePtr)[i][j];
                } else if ((*Watershed)[i][j] > 0.) {
                    (*RechargePtr)[i][j] = (*ActualNetRecharge)[count]();
                    ++count;
                }
            }
        }
    };


    void compute_one_step(const vd::Time& time) 
    {
        if (DbgLog > 0.) { 
            std::cout << std::setprecision(10) << time << "\t[Ambhas_cpp]\t\tRunning main function" << std::endl;
            if (DbgLog > 1.) {
                std::cout << "\tDBG!!\ttime=\t" << (double) time << std::endl;
                std::cout << "\t\t\thini:" << std::endl;
                std::cout << "------------------------------------------------------------" << std::endl;
                for (int i = 0; i < nrows; i++) { //This loops on the rows.
                    for (int j = 0; j < ncols; j++) { //This loops on the columns
                            std::cout <<std::setprecision(7)<< hini[i][j] << "  ";
                    }
                    std::cout << std::endl;
                }
                std::cout << "------------------------------------------------------------" << std::endl;
                std::cout << "\t\t\tnet_recharge:"  << std::endl;
                std::cout << "------------------------------------------------------------" << std::endl;
                for (int i = 0; i < nrows; i++) { //This loops on the rows.
                    for (int j = 0; j < ncols; j++) { //This loops on the columns
                            std::cout << mRecharge[i][j] << "  ";
                    }
                    std::cout << std::endl;
                }
                std::cout << "------------------------------------------------------------" << std::endl;
            }
        }

        double** hnew;
        double** dischargenew;
        double max_neumann_criterion;
        ambhas_gw_2d_xy(nrows, ncols, watershed, hini, mT, hmin, par_discharge, mRecharge, sy, dx, gw_dt, 1, hnew, dischargenew, max_neumann_criterion);
       
        updateVariables(&hnew, &dischargenew);
        free_Carrayptrs(hnew);
        free_Carrayptrs(dischargenew);
    };



    void updateVariables (double *** Hnew, double *** Dischargenew) 
    {
        if (DbgLog > 1.) {
            std::cout << "\t\t\thnew:" << std::endl;
            std::cout << "------------------------------------------------------------" << std::endl;
        }
        
        unsigned int counter = 0;
        for (int i = 0; i < nrows; ++i) {
            for (int j = 0; j < ncols; ++j) {
                if (watershed[i][j] > 0.) {
                    h1[counter] = std::min((*Hnew)[i][j], dem[i][j] - mSoilThickness);
                    if (DbgLog > 1.) std::cout << h1[counter]() << "  ";
                    hini[i][j] = h1[counter]();
                    gw_overland_flow[counter] = ((*Hnew)[i][j] - h1[counter]())* sy[i][j] * mGWpixelArea;
                    discharge[counter] = (*Dischargenew)[i][j];
//                    discharge[counter] = mRecharge[i][j]-(h1[counter]()-h1[counter](-1))*gw_Sy; //from mass balance equation                    
//                    discharge[counter] = (1.-gw_par_discharge)*(h1[counter](-1) + actual_net_recharge[counter]()/gw_Sy -gw_hmin)*gw_Sy; //from python equation     
                    gwvol[counter] = std::max(0., (h1[counter]() - hmin[i][j]) * sy[i][j] * mGWpixelArea);

                    if (h1[counter]()<0.) {
                        std::cout << "WARNING!!! negative value for h1(id=" << counter << ", row=" << i << ", col=" << j << ")=" << h1[counter]() <<
                        "\t=f(h1=" << h1[counter](-1) << ", recharge=" << actual_net_recharge[counter]() << ")" << std::endl;
                    }
                    ++counter;
                } else if (DbgLog > 1.) {
                    std::cout <<  "NA  ";
                }

            }
            if (DbgLog > 1.) std::cout << std::endl;
        }
        if (DbgLog > 1.) std::cout << "------------------------------------------------------------" << std::endl;

        if (DbgLog > 1.) {
            std::cout << "\t\t\tdischarge:" << std::endl;
            std::cout << "------------------------------------------------------------" << std::endl;
            counter=0;
            for (int i = 0; i < nrows; ++i) {
                for (int j = 0; j < ncols; ++j) {
                    if (watershed[i][j] > 0.) {
                        std::cout << discharge[counter]() << "  ";
                        ++counter;
                    } else {
                        std::cout <<  "NA  ";
                    }
                }
                std::cout << std::endl;
            }
        }

        if (DbgLog > 1.) {
            std::cout << "\t\t\toverland_flow:" << std::endl;
            std::cout << "------------------------------------------------------------" << std::endl;
            counter=0;
            for (int i = 0; i < nrows; ++i) {
                for (int j = 0; j < ncols; ++j) {
                    if (watershed[i][j] > 0.) {
                        std::cout << gw_overland_flow[counter]() << "  ";
                        ++counter;
                    } else {
                        std::cout <<  "NA  ";
                    }
                }
                std::cout << std::endl;
            }
        }

    };


//http://scipy-cookbook.readthedocs.io/items/C_Extensions_NumPy_arrays.html?highlight=c%20extension
    /* ==== Free a double *vector (vec of pointers) ========================== */ 
    void free_Carrayptrs(double **v) { 
        free((char*) v);
    };

};
};  //namespaces
DECLARE_DYNAMICS(Ambhas::Ambhas_cpp); // balise specifique VLE


