/**
 * @file src/GenGRDcan_cpp.cpp
 * @author Eric Casellas (The RECORD team -INRA)
 */

/*
 * Copyright (C) 2017-2018 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
// @@tagdynamic@@
// @@tagdepends: ext.Eigen @@endtagdepends


#include <vle/devs/Executive.hpp>

#include <vle/utils/Tools.hpp>
#include <vle/utils/Context.hpp>
#include <vle/utils/Package.hpp>
#include <vle/value/Matrix.hpp>
#include <vle/value/Set.hpp>


#include <fstream>
#include <iostream> // cout

#include <FnLib.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;
namespace vp = vle::vpz;

namespace Ambhas {

class GenGRDcan_cpp : public vd::Executive
{
public:
    GenGRDcan_cpp(const vd::ExecutiveInit& mdl, const vd::InitEventList& events) :
        vd::Executive(mdl, events)
    {
        getVPZParameters(events);

        checkVPZParametersValidity();
    }

        
    virtual ~GenGRDcan_cpp() {}
    
    virtual void finish()
    {
        if (DbgLog > 0.) std::cout << "\t[GenGRDcan_cpp]\tMemory cleaning start" << std::endl;
        free_Carrayptrs(GrdValues);
        if (DbgLog > 0.) std::cout << "\t[GenGRDcan_cpp]\tMemory cleaning start" << std::endl;
    }

    virtual vd::Time init(vd::Time /*time*/) override
    {
        getGRDParameters();

        addAllParcellesWithConditions();

        editRelaiPorts();

        connectIO();

        dumpVPZ();

        testWriteGRD();

        return vd::infinity;
    }

private:
    //paramètres VPZ
    std::string GRDFileName; //Nom du fichier SIG
    std::string RelaiName; //Nom du modèle Relai
    std::string ParcelleClassName; //Nom de la classe du vpz à utiliser pour les modèles Parcelle
    std::string ParcellePrefix; //Prefixe des instances Parcelle
    std::string R_inputPortsInd; //Noms des ports d'entrée du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature)
    std::string R_inputPortsCom; //Noms des ports d'entrée du modèle Relai qui sont communes à toutes les Parcelles
    std::string R_outputPortsInd; //Noms des ports de sortie du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature)
    std::string R_outputPortsCom; //Noms des ports de sortie du modèle Relai qui sont communes à toutes les Parcelles
    std::string R_Condition; //Nom de la condition de la classe cRelai qui contient le paramètre n
    std::string P_Conditions; //Nom de la condition de la classe cParcelle qui contient les paramètres spécifiques à chaque parcelle
    std::string P_ParamPorts; //Noms des ports la condition P_Conditions de la classe cParcelle des paramètres spécifiques à chaque parcelle
    std::string P_ParamPortsType; //Types des ports la condition P_Conditions de la classe cParcelle des paramètres spécifiques à chaque parcelle
    double DbgLog; //Verbosité des sorties
    std::string ConditionSeparator; //séparateur utilisé pour séparer les différents elements des listes contenus dans les string
    std::string P_Condition_id; //Nom de la condition de la classe cParcelle qui contient le paramètre P_Condition_idPort identifiant spécifiques à chaque parcelle
    std::string P_Condition_idPort; //Nom du port de la condition P_Condition_id qui contient le paramètre identifiant spécifiques à chaque parcelle
    std::string P_Condition_idPortType; //Type du port P_Condition_idPort de la condition P_Condition_id
    std::string PackageName; //Nom du paquet installé contenant le code python (utile si renommage par rapport à celui originalement prevu: TestCluster_Stics)
    bool AddObservable;
    std::string NamingMode; //Type de convention pour le nommage des identifiants parcelle (id unique ou couple x_y)
    std::string DumpFileName;
    std::string ObservableName;
    std::string ViewName;
    std::vector<std::string> ObservablePortNames;

    std::string RelaiClass;
        


    //Variables locales
    double** GrdValues;
    int nrows, ncols;
    unsigned int n; //nombre de parcelles définies dans le fichier SIG
    std::vector<std::string> r_inputPortsInd; //vecteur de R_inputPortsInd
    std::vector<std::string> r_inputPortsCom; //vecteur de R_inputPortsCom
    std::vector<std::string> r_outputPortsInd; //vecteur de R_outputPortsInd
    std::vector<std::string> r_outputPortsCom; //vecteur de R_outputPortsCom
    std::vector<std::string> p_ParamPorts; //vecteur de P_ParamPorts
    std::vector<std::string> p_ParamPortsType; //vecteur de P_ParamPortsType
    std::vector<std::string> p_ParamPortsDBName; //vecteur de P_ParamPortsDBName
    std::vector<std::string> p_Conditions; //vecteur de P_Conditions
    std::vector<std::vector<vv::Value> > p_ParamPortsValues; //vecteur de P_ParamPortsValues
    vv::Matrix p; //stocke les valeurs de paramètres lues dans le fichier sig (colonnes=parametres, ligne=parcelles)



    //séparation des conditions en vecteurs
    std::vector<std::string> stringParser (const std::string & Msg, const std::string & Separators)
        {
            std::vector< std::string > lst;
            vu::tokenize(Msg, lst, Separators, true);
            return lst;
        }

    //Lecture configuration de l'Executive (paramètres VPZ)
    void getVPZParameters(const vd::InitEventList& events)
    {
        DbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.;
        GRDFileName = vv::toString(events.get("GRDFileName"));
        
        RelaiClass = (events.exist("RelaiClass")) ? vv::toString(events.get("RelaiClass")) : "AmbhasClass";
        RelaiName = (events.exist("RelaiName")) ? vv::toString(events.get("RelaiName")) : "Relai";
        ParcelleClassName = (events.exist("ParcelleClassName")) ? vv::toString(events.get("ParcelleClassName")) : "cParcelle";
        ParcellePrefix = (events.exist("ParcellePrefix")) ? vv::toString(events.get("ParcellePrefix")) : "Parcelle_";

        ConditionSeparator = (events.exist("ConditionSeparator")) ? vv::toString(events.get("ConditionSeparator")) : ";";

        R_inputPortsInd = (events.exist("R_inputPortsInd")) ? vv::toString(events.get("R_inputPortsInd")) : "";
        R_inputPortsCom = (events.exist("R_inputPortsCom")) ? vv::toString(events.get("R_inputPortsCom")) : "";
        R_outputPortsInd = (events.exist("R_outputPortsInd")) ? vv::toString(events.get("R_outputPortsInd")) : "";
        R_outputPortsCom = (events.exist("R_outputPortsCom")) ? vv::toString(events.get("R_outputPortsCom")) : "";

        R_Condition = (events.exist("R_Condition")) ? vv::toString(events.get("R_Condition")) : "";

        P_Conditions = (events.exist("P_Conditions")) ? vv::toString(events.get("P_Conditions")) : "";
        P_ParamPorts = (events.exist("P_ParamPorts")) ? vv::toString(events.get("P_ParamPorts")) : "";
        P_ParamPortsType = (events.exist("P_ParamPortsType")) ? vv::toString(events.get("P_ParamPortsType")) : "";

        P_Condition_id = (events.exist("P_Condition_id")) ? vv::toString(events.get("P_Condition_id")) : "";
        P_Condition_idPort = (events.exist("P_Condition_idPort")) ? vv::toString(events.get("P_Condition_idPort")) : "id";
        P_Condition_idPortType = (events.exist("P_Condition_idPortType")) ? vv::toString(events.get("P_Condition_idPortType")) : "integer";

        PackageName = (events.exist("PkgName")) ? vv::toString(events.get("PkgName")) : "Ambhas_GW";

        NamingMode = (events.exist("NamingMode")) ? vv::toString(events.get("NamingMode")) : "x_y";
        DumpFileName = (events.exist("DumpFileName")) ? vv::toString(events.get("DumpFileName")) : "dump_GenGRDcan_cpp.vpz";
        
        AddObservable = (events.exist("AddObservable")) ? vv::toBoolean(events.get("AddObservable")) : false;
        if (AddObservable) {
            ObservableName = (events.exist("ObservableName")) ? vv::toString(events.get("ObservableName")) : "obs";
            ViewName = (events.exist("ViewName")) ? vv::toString(events.get("ViewName")) : "vue";
            if (events.exist("ObservablePortNames")) {
                const vv::Set& obsset = events.getSet("ObservablePortNames");
                for (unsigned int index = 0; index < obsset.size(); ++index) {
                    ObservablePortNames.push_back(obsset.getString(index));
                }
            } else {
                ObservablePortNames.push_back("H");
            }
        }

        //Vectorisation des conditions
        r_inputPortsInd = stringParser(R_inputPortsInd, ConditionSeparator);
        r_inputPortsCom = stringParser(R_inputPortsCom, ConditionSeparator);
        r_outputPortsInd = stringParser(R_outputPortsInd, ConditionSeparator);
        r_outputPortsCom = stringParser(R_outputPortsCom, ConditionSeparator);
        p_ParamPorts = stringParser(P_ParamPorts, ConditionSeparator);
        p_ParamPortsType = stringParser(P_ParamPortsType, ConditionSeparator);
        p_Conditions = stringParser(P_Conditions, ConditionSeparator);
    }

    //Vérification de la validité de la configuration en fonction du format attendu
    void checkVPZParametersValidity()
    {
        //Vérification de la présence du parametre obligatoire ParcelleClassName
        if (ParcelleClassName.empty()) {
            std::stringstream ss;
            ss << "Invalid Parameter!!\nParcelleClassName shall not be empty" << std::endl;
            throw vu::ModellingError(ss.str());
        }

        //Vérification de la validité du format du fichier Grid (*.grd)
        if (GRDFileName.find(".grd") == std::string::npos) {
            std::stringstream ss;
            ss << "Invalid Parameter!!\nGRDFileName :" << GRDFileName
               << "\nFile format not handled by the model.\nChoose a file with .grd extension" << std::endl;
            throw vu::ModellingError(ss.str());
        }

        //Verification du même nombre d'éléments dans les paramètres P_Conditions, P_ParamPorts, P_ParamPortsType et P_ParamPortsDBName
        if (!((p_ParamPorts.size() == p_ParamPortsType.size())
                && (p_ParamPorts.size() == p_Conditions.size()))) {
            std::stringstream ss;
            ss << "Invalid Parameters!! All should have the same number of elements :\nP_ParamPorts :"
               << p_ParamPorts.size() << "\nP_ParamPortsType :"
               << p_ParamPortsType.size() << "\nP_Conditions :" << p_Conditions.size() << std::endl;
            throw vu::ModellingError(ss.str());
        }

        //Vérification si les types de P_ParamPortsType sont pris en charge par le modèle
        for (unsigned int j = 0; j < p_ParamPortsType.size(); j++) { //boucle sur les différents paramètres
            if ((p_ParamPortsType[j] != "double") && (p_ParamPortsType[j] != "integer")) {
                std::stringstream ss;
                ss << "Invalid Parameter!!\nP_ParamPortsType :" << p_ParamPortsType[j]
                        << "\nData type not handled by the model. Will use string\nChoose values from : { double ; integer }" << std::endl;
                throw vu::ModellingError(ss.str());
            }
        }
    }

    //Lecture du fichier SIG et recupération des informations (paramètres spécifiques et nbr de parcelles)
    // allocate memory to GrdValues
    void getGRDParameters()
    {
        auto ctx = vu::make_context();
        vu::Package mPack(ctx, PackageName);
        std::string GRDfilepath = mPack.getDataFile(GRDFileName);

        std::map<std::string,double> param ;
        readGrd(GrdValues, param, GRDfilepath);
        nrows=(int)param["nrows"];
        ncols=(int)param["ncols"];
        
        if (DbgLog >= 1.) {
            std::cout << "\t[GenGRDcan_cpp]\t\tRead watershed description : " << GRDfilepath << std::endl;
            std::cout << "\t[GenGRDcan_cpp]\t\t\tFound nrows :" << nrows << std::endl;
            std::cout << "\t[GenGRDcan_cpp]\t\t\tFound ncols :" << ncols << std::endl;
        }

        p.clear();
//            for (unsigned int i = 0; i < p_ParamPorts.size(); i++)
        p.addColumn();
        n = 0;
        if (DbgLog >= 1.) std::cout << "Watershed pixels :" << std::endl;
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;
        for (int i = 0; i < nrows; i++) { //This loops on the rows.
            for (int j = 0; j < ncols; j++) { //This loops on the columns
                if (DbgLog >= 1.) std::cout << GrdValues[i][j] << "  ";
                if (GrdValues[i][j] > 0.) { //we count as non-empty codes !=0
                    ++n;
                    p.addRow();
                }
             }
            if (DbgLog >= 1.) std::cout << std::endl;
        }
        if (DbgLog >= 1.) std::cout << "------------------------------------------------------------" << std::endl;


        if (DbgLog >= 1.) std::cout << "\t[GenGRDcan_cpp]\t\tNon-empty cells number :" << n << std::endl;

        int count = 0;
        for (int i = 0; i < nrows; i++) { //This loops on the rows.
            for (int j = 0; j < ncols; j++) { //This loops on the columns
                if (p_ParamPortsType[0] == "double") {
                    if (GrdValues[i][j] > 0.) { //we count as non-empty codes !=0
                        if (DbgLog >= 2.) std::cout << "\t[GenGRDcan_cpp]\t\t\t" << count << " ->\t"
                                << p_ParamPorts[0] << " =\t" << GrdValues[i][j] << std::endl;
                        p.add(0, count, vv::Double::create(GrdValues[i][j]));
                        ++count;
                    }
                } else if (p_ParamPortsType[0] == "integer") {
                    if (GrdValues[i][j] > 0.) { //we count as non-empty codes !=0
                        if (DbgLog >= 2.) std::cout << "\t[GenGRDcan_cpp]\t\t\tid:" << count 
                                << " (x=" << j << ", y=" << i << ") ->\t"
                                << p_ParamPorts[0] << " =\t" << GrdValues[i][j] << std::endl;
                        p.add(0, count, vv::Integer::create(GrdValues[i][j]));
                        ++count;
                    }
                } else {
                    if (GrdValues[i][j] > 0.) { //we count as non-empty codes !=0
                        std::cout << "\t[GenGRDcan_cpp]\t\t\t" << count << " ->\t"
                                << p_ParamPorts[0] << " =\t" << GrdValues[i][j] << std::endl;
                        p.add(0, count, vv::Double::create(GrdValues[i][j]));
                        ++count;
                    }
                }
            }
        }

    }

    void addAllParcellesWithConditions()
    {
        
        if (AddObservable) {
            for (unsigned int i = 0; i < ObservablePortNames.size(); i++) { 
                editRelaiObs(ObservableName, ViewName, ObservablePortNames[i], "_");
            }
        }

        
        vp::Conditions& cond_l = conditions(); //référence aux conditions du VPZ

        //Modification de la condition contenant le paramètre n du modèle couplé Relai
        if (!R_Condition.empty()) {
            vp::Condition& cond_Relai = cond_l.get(R_Condition); //référence à la condition R_Condition de la classe cRelai contenant le paramètre n
            cond_Relai.setValueToPort("n", vv::Integer::create(n)); //modification de la valeur du port n de la condition R_Condition
            if (DbgLog >= 2.)
                std::cout << "\t[GenGRDcan_cpp]\tEditting relay condition\n\t[GenGRDcan_cpp]\t\t["
                        << R_Condition << "].n = " << n << std::endl;
        } else if (DbgLog >= 2.)
            std::cout << "\t[GenGRDcan_cpp]\tNo condition editting associated to relay" << std::endl;
        
        std::cout << "\t[GenGRDcan_cpp]\tAdding plot model "
                                << RelaiName << " from class " << RelaiClass << std::endl;
        createModelFromClass(RelaiClass, RelaiName);

        int count = 0;
        for (int i = 0; i < nrows; ++i) {
            for (int j = 0; j < ncols; ++j) {
                if (GrdValues[i][j] > 0.) {
                    std::stringstream ss;
                    ss << ParcellePrefix << i << "_" << j;
                    if (DbgLog >= 2.) {
                        std::cout << "\t[GenGRDcan_cpp]\tEditting condition associated to plot " << ss.str() << std::endl;
                    }
                    //Modification de la condition P_Condition_id contenant le paramètre P_Condition_idPort de chaque Parcelle
                    if (!P_Condition_id.empty()) {
                        vp::Condition& cond_Parcelle = cond_l.get(P_Condition_id);
                        if (P_Condition_idPortType == "integer") {
                            cond_Parcelle.setValueToPort(P_Condition_idPort, vv::Integer::create(count));
                        } else {
                            std::stringstream ss;
                            ss << count;
                            cond_Parcelle.setValueToPort(P_Condition_idPort, vv::String::create(ss.str()));
                        }
                        if (DbgLog >= 2.)
                            std::cout << "\t[GenGRDcan_cpp]\t\t[" << P_Condition_id << "]." << P_Condition_idPort << " = " << count << std::endl;
                    }
                    //Modification Conditions spécifiques de chaque Parcelle
                    if (p_ParamPorts.size() > 0) {
                        for (unsigned int j = 0; j < p_ParamPorts.size(); j++) { //boucle sur les différents paramètres
                            std::stringstream valstr;
                            vp::Condition& cond_Parcelle = cond_l.get(p_Conditions[j]); //référence à la condition p_Conditions  contenant les paramètres spécifiques des parcelles
                            if (p_ParamPortsType[j] == "double") {
								const double v = p.getDouble(0,count);
								cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::Double::create(v));
								valstr << v;
							} else if (p_ParamPortsType[j] == "integer") {
								const int v = p.getInt(0,count);
								cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::Integer::create(v));
								valstr << v;
							} else if (p_ParamPortsType[j] == "string") {
								const std::string v = p.getString(0,count);
								cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::String::create(v));
								valstr << v;
							} else {
								const std::string v = p.getString(0,count);
								cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::String::create(v));
								valstr << v;
							}
                            if (DbgLog >= 2.)
                                std::cout << "\t[GenGRDcan_cpp]\t\t[" << p_Conditions[j] << "]."
                                        << p_ParamPorts[j] << " = " << valstr.str() << std::endl;
                        }
                    } else if (DbgLog >= 2.)
                        std::cout << "\t[GenGRDcan_cpp]\tNo condition editting associated to plot "
                                << ss.str() << std::endl;

                    if (DbgLog >= 2.)
                        std::cout << "\t[GenGRDcan_cpp]\tAdding plot model "
                                << ss.str() << " from class " << ParcelleClassName << std::endl;
                    vd::Executive::createModelFromClass(ParcelleClassName, ss.str());
                    ++count;
                }
            }
        }
    }

    //Creation des ports d'entrée/sortie du modèle Relai
    void editRelaiPorts()
    {
        if ((r_outputPortsCom.size() > 0) || (r_inputPortsCom.size() > 0)) {
            if (DbgLog >= 2.)
                std::cout << "\t[GenGRDcan_cpp]\tAdding common ports to relay model : "
                        << RelaiName << std::endl;
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) { //boucle sur les différentes sorties communes entre parcelles
                AddOutputPort(RelaiName, r_outputPortsCom[j]);
            }
            for (unsigned int j = 0; j < r_inputPortsCom.size(); j++) { //boucle sur les différentes entrées communes entre parcelles
                AddInputPort(RelaiName, r_inputPortsCom[j]);
            }
        }

        if ((r_outputPortsInd.size() > 0) || (r_inputPortsInd.size() > 0)) {
            if (DbgLog >= 2.)
                std::cout << "\t[GenGRDcan_cpp]\tAdding individual ports to relay model : "
                        << RelaiName << std::endl;
            unsigned int counter = 0;
            for (int i = 0; i < nrows; ++i) {
                for (int j = 0; j < ncols; ++j) {
                    if (GrdValues[i][j] > 0.) {
                        for (unsigned int k = 0; k < r_outputPortsInd.size(); k++) { //boucle sur les différentes sorties spécifiques aux parcelles
                            std::stringstream ss2;
                            if (NamingMode == "x_y") {
                                ss2 << r_outputPortsInd[k] << "_" << i << "_" << j;
                            } else {
                                ss2 << r_outputPortsInd[k] << "_" << counter;
                            }
                            AddOutputPort(RelaiName, ss2.str());
                        }
                        for (unsigned int k = 0; k < r_inputPortsInd.size(); k++) { //boucle sur les différentes entrées spécifiques aux parcelles
                            std::stringstream ss2;
                            if (NamingMode == "x_y") {
                                ss2 << r_inputPortsInd[k] << "_" << i << "_" << j;
                            } else {
                                ss2 << r_inputPortsInd[k] << "_" << counter;
                            }
                            AddInputPort(RelaiName, ss2.str());
                        }
                        ++counter;
                    }
                }
            }
        }
    }

    void AddInputPort(const std::string &model, const std::string &inputport)
    {
        if (DbgLog >= 2.)
            std::cout << "\t[GenGRDcan_cpp]\t\t(input) \t" << inputport << std::endl;
        vd::Executive::addInputPort(model, inputport);
    }

    void AddOutputPort(const std::string &model, const std::string &outputport)
    {
        if (DbgLog >= 2.)
            std::cout << "\t[GenGRDcan_cpp]\t\t(output)\t" << outputport << std::endl;
        vd::Executive::addOutputPort(model, outputport);
    }

    //Connections des Entrées/Sorties entre le modèle Relai et les n modèles Parcelle
    void connectIO()
    {
        if ((r_outputPortsCom.size() > 0) || (r_inputPortsCom.size() > 0)
                || (r_outputPortsInd.size() > 0)
                || (r_inputPortsInd.size() > 0)) {
            if (DbgLog >= 2.)
                std::cout << "\t[GenGRDcan_cpp]\tConnecting inputs/outputs between relay and plot models : " << std::endl;
            unsigned int counter = 0;
            for (int i = 0; i < nrows; ++i) {
                for (int j = 0; j < ncols; ++j) {
                    if (GrdValues[i][j] > 0.) {
                        std::stringstream ss;
                        ss << ParcellePrefix << i << "_" << j;
                        if (DbgLog >= 2.)
                            std::cout << "\t[GenGRDcan_cpp]\t\tConnecting plot model " << ss.str() << std::endl;
                        for (unsigned int k = 0; k < r_outputPortsCom.size(); k++) //boucle sur les différentes sorties communes entre parcelles
                            AddConnection(RelaiName, r_outputPortsCom[k], ss.str(), r_outputPortsCom[k]);
                        for (unsigned int k = 0; k < r_inputPortsCom.size(); k++) //boucle sur les différentes entrées communes entre parcelles
                            AddConnection(ss.str(), r_inputPortsCom[k], RelaiName, r_inputPortsCom[k]);
                        for (unsigned int k = 0; k < r_outputPortsInd.size(); k++) { //boucle sur les différentes sorties spécifiques aux parcelles
                            std::stringstream ss2;
                            if (NamingMode == "x_y") {
                                ss2 << r_outputPortsInd[k] << "_" << i << "_" << j;
                            } else {
                                ss2 << r_outputPortsInd[k] << "_" << counter;
                            }
                            AddConnection(RelaiName, ss2.str(), ss.str(), r_outputPortsInd[k]);
                        }
                        for (unsigned int k = 0; k < r_inputPortsInd.size(); k++) { //boucle sur les différentes entrées spécifiques aux parcelles
                            std::stringstream ss2;
                            if (NamingMode == "x_y") {
                                ss2 << r_inputPortsInd[k] << "_" << i << "_" << j;
                            } else {
                                ss2 << r_inputPortsInd[k] << "_" << counter;
                            }
                            AddConnection(ss.str(), r_inputPortsInd[k], RelaiName, ss2.str());
                        }
                        ++counter;
                    }
                }
            }
        } else if (DbgLog >= 2.)
            std::cout << "\t[GenGRDcan_cpp]\tNo inputs/outputs connections between relay and plot models." << std::endl;
    }

    void AddConnection(const std::string &modelsource,
            const std::string &outputport, const std::string &modeldestination,
            const std::string &inputport)
    {
        if (DbgLog >= 2.)
            std::cout << "\t[GenGRDcan_cpp]\t\t\t[" << modelsource << "]."
                    << outputport << "\t->\t[" << modeldestination << "]."
                    << inputport << std::endl;
        vd::Executive::addConnection(modelsource, outputport, modeldestination, inputport);
    }

    void dumpVPZ()
    {
        if (DbgLog >= 1.) {
            std::cout << "\t[GenGRDcan_cpp]\tDumping file : " << DumpFileName << std::endl;
            std::ofstream file(DumpFileName.c_str());
            dump(file, "dump");
        }
    }

    void testWriteGRD()
    {

    }

    void editRelaiObs(std::string obsName="obs", std::string vueName = "vue", std::string varPrefix = "H", std::string varSep="_")
    {
        if (DbgLog >= 1.) {
            std::cout << "\t[GenGRDcan_cpp]\tEditing Relai model observations..." << std::endl;
        }
        vp::Observable& myObs = observables().get(obsName);
        
        if (n > 1) {
            unsigned int counter = 0;
            for (int i = 0; i < nrows; i++) { //This loops on the rows.
                for (int j = 0; j < ncols; j++) { //This loops on the columns
                    if (GrdValues[i][j] > 0.) {
                        std::stringstream ss;
                        if (NamingMode == "x_y") {
                            ss << varPrefix << varSep << i << varSep << j;
                        } else {
                            ss << varPrefix << varSep << counter;
                        }
                        vp::ObservablePort myPort(ss.str());
                        myPort.add(vueName);
                        myObs.add(myPort);
                        if (DbgLog >= 2.) {
                            std::cout << "\t[GenGRDcan_cpp]\t\tAdding port " << ss.str() << " to observable " << obsName 
                                << " (attached to view " << vueName << ")" << std::endl;
                        }
                    ++counter;
                    }
                }
            }
        } else {
            vp::ObservablePort myPort(varPrefix);
            myPort.add(vueName);
            myObs.add(myPort);
            if (DbgLog >= 2.) {
                std::cout << "\t[GenGRDcan_cpp]\t\tAdding port " << varPrefix << " to observable " << obsName 
                          << " (attached to view " << vueName << ")" << std::endl;
            }
        }
    }

//http://www.scipy.org/Cookbook/C_Extensions/NumPy_arrays#head-44f570266d5f18df266252b0b98b73a5921468a3
    /* ==== Free a double *vector (vec of pointers) ========================== */ 
    void free_Carrayptrs(double **v) { 
        free((char*) v);
    }


};
}
DECLARE_EXECUTIVE (Ambhas::GenGRDcan_cpp);


