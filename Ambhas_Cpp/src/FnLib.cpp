#include "FnLib.hpp"
#include <fstream> 
#include <iostream> 
#include <vector>
#include <iomanip>      // std::setprecision

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>


typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<double> Triplt;

namespace Ambhas {

    void readGrd(MatrixXd& m, std::map<string,double>& hash, string grdfile){

        std::fstream fs(grdfile.c_str(),std::fstream::in); // flux le lecture sur le fichier grdfile
        if (!fs){
          cerr<<"erreur ouverture  en lecture du fichier "<<grdfile<<endl ;
          return;
        }
        
        string header_name ; // pour stocker le nom des parametres
        double value ; // pour stocker la valeure des parametres

        // Lecture des cinqs premiers parametres du fichier grd : grdfile
        for (int i= 0 ;i < 5 ; i++){
            fs>>header_name>>value ;
            hash[header_name] = value ;
        }
        
        vector<double> flat_matrice ;
        
        // Tentative de lecture du parametre "NODATA_value" si il existe
        fs>>header_name>>value;
        if(header_name.compare("NODATA_value") == 0){
            hash[header_name] = value ;
        }else{
          hash["NODATA_value"] = no_data_value;
          stringstream ss;
          ss<<header_name;
          double value1;
          ss>>value1 ;
          flat_matrice.push_back(value1) ;
          flat_matrice.push_back(value) ;
        }
        
        // lecture de l'ensemble des valeurs de la grille    
        while (fs>>value){
            flat_matrice.push_back(value) ;
        }
        
        // transfère des valeurs de la grille dans la matrice m
        unsigned int nrows = (int)hash["nrows"];
        unsigned int ncols = (int)hash["ncols"];
        
        // verification de la cohérence entre nrows, ncols et la matrice
        if (nrows * ncols != flat_matrice.size()){
            std::cerr<< "erreur incoherence du fichier grd ... " << endl << nrows << " rows * "<<ncols<<" cols != "<<flat_matrice.size()<<endl ; 
        }
        
        m = MatrixXd(nrows,ncols) ;
        for (unsigned int i = 0 ;i < flat_matrice.size(); i++){
          m((i-(i%ncols))/ncols,i%ncols) = flat_matrice[i] ;
        }

        fs.close();
    }

    void readGrd(double**& m, std::map<string,double>& hash, string grdfile){
        MatrixXd mat;
        readGrd(mat, hash, grdfile);
        
        int nrows = (int)hash["nrows"];
        int ncols = (int)hash["ncols"];
        int no_data_value = (int)hash["NODATA_value"];
        
        //set m (double**) values from mat (MatrixXd)
        // set m to 0 for NA values
        m = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            m[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
                m[i][j] = mat(i,j);
                if (m[i][j] == no_data_value){
                    m[i][j] = 0 ;
                }
            }
        }
    }
    
    
    /*
    Input:
    *     nrows, ncols, : number of rows and cols in the watershed matrice
        watershed:  map of watershed; 1 means inside and 0 means outside watershed
        hini:       initial groundwater level
        T:          transmissivity as a matrix
        Sy:         specific yield
        dx:         cell size (in m for square shape)
        dt:         time step in seconds
        hmin:       groundwater level corresponding to zero discharge
        par_discharge: parameter controlling the discharge (range is 0 to 1) 
        net_recharge:   net recharge to the groundwater (recharge-groundwater pumping)
        alpha:      parameter controlling resolution algo (1.0 ~ implicit) 
    
    Output:
        hnew:       groundwater level at next time step
        discharge:  baseflow 
        max_neumann_criterion: value of the maximum neumann number
     */
       
    void ambhas_gw_2d_xy(
            int nrows,
            int ncols,
            MatrixXd& watershed, 
            MatrixXd& hini, 
            const MatrixXd& T, 
            const MatrixXd& hmin, 
            const MatrixXd& par_discharge, 
            const MatrixXd& net_recharge, 
            const MatrixXd& Sy, 
            const double dx, 
            const double dt,
            const double alpha,
            MatrixXd& hnew,
            MatrixXd& discharge,
            double& max_neumann_criterion)
    {
/// code python:         
///      d = dx # spatial resolution of the model
///   D = T/Sy
        double d = dx ;
        MatrixXd D = (T.array()/Sy.array()).matrix() ;

//        remarque, dans le code python il y a une incohérence avec le commentaire
//        Le commentaire indique :  neumann_criterion = 0.5*d**2/D
//         alors que le code python indique: neumann_criterion = 2*D*dt/d**2
///       neumann_criterion = 2*D*dt/d**2
        MatrixXd neumann_criterion = D * (2*dt/(d*d)) ;
        
///    max_neumann_criterion = neumann_criterion.max()
        max_neumann_criterion = neumann_criterion.maxCoeff(); // http://eigen.tuxfamily.org/dox/group__TutorialMatrixArithmetic.html
//        std::cout<<"max_neumann_criterion= "<<max_neumann_criterion<<std::endl ;
        
//    # update the model for recharge
///    hini = hini + net_recharge/Sy
        hini = hini + (net_recharge.array()/Sy.array()).matrix() ;
        
//    # take the discharge out from each cell
///    discharge = (1-par_discharge)*(hini-hmin)*Sy
        discharge = ((1-par_discharge.array()) * (hini.array()-hmin.array()) * Sy.array()).matrix() ; // using array coefficient-wise operations. http://eigen.tuxfamily.org/dox/group__TutorialArrayClass.html
        
///    discharge[hini<hmin] = 0
        discharge = discharge.unaryExpr(ptr_fun(min0)); // using unary operator coefficient-wise http://eigen.tuxfamily.org/dox/classEigen_1_1MatrixBase.html#acbe602d55aa168bfbc48b71524f0710b

///    hini = hini - discharge/Sy
        hini = hini - (discharge.array()/Sy.array()).matrix() ;
    
//    # spatial computing    
///   n = int(np.sum(watershed))
///   foo.shape = watershed.shape
///      foo = np.cumsum(watershed.flatten())
///   foo[watershed==0] = np.nan
///   foo = foo-1 # indices start from 0

        int n =0 ;
        MatrixXd foo(nrows, ncols) ;
        
        // pour remplacer "np.nan" du code python
        const int foo_out_watershed = -1 ;
        for (int i = 0 ; i < nrows; i++){
            for (int j = 0 ; j < ncols; j++){
                n += watershed(i,j) ;
                if (watershed(i,j) != 0){
                    foo(i,j) = n - 1 ;
                }else{
                    foo(i,j) = foo_out_watershed ;
                }
            }
        }


/// ih, jh = np.unravel_index(find(~np.isnan(foo)), watershed.shape) # indices of non nan

        VectorXd ih(n), jh(n) ;
        int indice_n = 0 ;
        for (int i = 0 ; i < nrows; i++){
            for (int j = 0 ; j < ncols; j++){
                if (watershed(i,j)!=0){
                    ih(indice_n) = i ;
                    jh(indice_n) = j ;
                    indice_n ++ ;
                }
            }
        }
    
///        # setup A and b matrix    
///    A = lil_matrix((n, n))
///    b = np.zeros(n,)
        SpMat A(n,n) ;
        VectorXd b = VectorXd::Zero(n) ;
        
        std::vector<Triplt> coefficients; // list of non-zeros coefficients
        
        double value_i_i = 0, value_im1_i = 0, value_ip1_i = 0, value_jm1_i = 0, value_jp1_i = 0 ;
        double ind_h_im1 = 0  ,ind_h_ip1 = 0, ind_h_jm1 = 0, ind_h_jp1= 0 ;
///    for i in xrange(n):
        for (int i = 0 ;i < n ; i++){
///        a1 = alpha/d**2
///        a2 = -(4*alpha/d**2 + Sy/T[ih[i],jh[i]]/dt)
            double a1 = alpha/(d*d) ;
            double a2 = -(4* a1 + Sy(ih(i),jh(i))/T(ih(i),jh(i))/dt) ;
            double a3 = (1-alpha)/(d*d);
    
///       # i,j
///           A[i,i] = a2
///            b[i] = (-4*(1-alpha)/d**2 - Sy/T[ih[i],jh[i]]/dt)*hini[ih[i],jh[i]]
            value_i_i = a2 ;
            
            b(i) = (-4*a3 -Sy(ih(i),jh(i))/T(ih(i),jh(i))/dt)*hini(ih(i), jh(i)) ;
            
///        # i-1,j
///        ind_h = foo[ih[i]-1,jh[i]]
            double ind_h ;
            // test util pour la premiere ligne
            if (ih(i)-1 >= 0){
                ind_h = foo(ih(i)-1,jh(i)) ;
            }else{
                ind_h = foo_out_watershed ;
            }
            ind_h_im1 = ind_h ;
            
///        if np.isnan(ind_h):
///            A[i,i] = A[i,i] + a1
///            b[i] = b[i] + (1-alpha)/d**2*hini[ih[i],jh[i]]
///        else:
///            A[i,int(ind_h)] = a1
///            b[i] = b[i] + (1-alpha)/d**2*hini[ih[i]-1,jh[i]]
            if (ind_h == foo_out_watershed){
                value_i_i += a1 ;
                b(i) += a3*hini(ih(i),jh(i)) ;
            }else{
                value_im1_i = a1 ;
                b(i) += a3*hini(ih(i)-1,jh(i)) ;
            } 
            
 ///       # i+1, j
///        ind_h = foo[ih[i]+1,jh[i]]
            if (ih(i)+1 <= n){
                ind_h = foo(ih(i)+1,jh(i)) ;
            }else{
                ind_h = foo_out_watershed ;
            }
            ind_h_ip1 = ind_h ;
///        if np.isnan(ind_h):
///            A[i,i] = A[i,i] + a1
///            b[i] = b[i] + (1-alpha)/d**2*hini[ih[i],jh[i]]
///        else:
///           A[i,int(ind_h)] = a1
///            b[i] = b[i] + (1-alpha)/d**2*hini[ih[i]+1,jh[i]]
            if (ind_h == foo_out_watershed){
                value_i_i += a1 ;
                b(i) += a3*hini(ih(i),jh(i)) ;
            }else{
                value_ip1_i = a1 ;
                b(i) += a3*hini(ih(i)+1,jh(i)) ;
            } 
            
 ///       # i, j-1
 ///       ind_h = foo[ih[i],jh[i]-1]
             if (jh(i)-1 >= 0){
                ind_h = foo(ih(i),jh(i)-1) ;
            }else{
                ind_h = foo_out_watershed ;
            }
            ind_h_jm1 = ind_h ;
 
 ///       if np.isnan(ind_h):
 ///           A[i,i] = A[i,i] + a1
 ///           b[i] = b[i] + (1-alpha)/d**2*hini[ih[i],jh[i]]
 ///       else:
 ///           A[i,int(ind_h)] = a1
 ///           b[i] = b[i] + (1-alpha)/d**2*hini[ih[i],jh[i]-1]
            if (ind_h == foo_out_watershed){
                value_i_i += a1 ;
                b(i) += a3*hini(ih(i),jh(i)) ;
            }else{
                value_jm1_i = a1 ;
                b(i) += a3*hini(ih(i),jh(i)-1) ;
            } 
            
 ///      # i, j+1
 ///     ind_h = foo[ih[i],jh[i]+1]
            if (jh(i)+1 <= n){
                ind_h = foo(ih(i),jh(i)+1) ;
            }else{
                ind_h = foo_out_watershed ;
            }
            ind_h_jp1 = ind_h ;
 
 ///      if np.isnan(ind_h):
 ///          A[i,i] = A[i,i] + a1
 ///          b[i] = b[i] + (1-alpha)/d**2*hini[ih[i],jh[i]]
 ///       else:
 ///          A[i,int(ind_h)] = a1
 ///          b[i] = b[i] + (1-alpha)/d**2*hini[ih[i],jh[i]+1]
            if (ind_h == foo_out_watershed){
                value_i_i += a1 ;
                b(i) += a3*hini(ih(i),jh(i)) ;
            }else{
                value_jp1_i = a1 ;
                b(i) += a3*hini(ih(i),jh(i)+1) ;
            } 
            
            coefficients.push_back(Triplt(i,i,value_i_i)) ;
            if (ind_h_im1 >=0){
                coefficients.push_back(Triplt(i,ind_h_im1,value_im1_i)) ;
            }
            if (ind_h_ip1 >=0){
                coefficients.push_back(Triplt(i,ind_h_ip1,value_ip1_i)) ;
            }
            if (ind_h_jm1 >=0){
                coefficients.push_back(Triplt(i,ind_h_jm1,value_jm1_i)) ;
            }
            if (ind_h_jp1 >=0){
                coefficients.push_back(Triplt(i,ind_h_jp1,value_jp1_i)) ;
            }
        }
        A.setFromTriplets(coefficients.begin(), coefficients.end()) ;
        
        /// tmp = spsolve(A.tocsr(),b)
        SimplicialLDLT <SpMat> solver ;
        
        solver.compute(A) ;
        VectorXd x = VectorXd(n) ;
        x = solver.solve(b) ;
        
        /// hnew = np.zeros(watershed.shape)
        /// hnew[ih,jh] = tmp
        /// hnew[watershed==0] = np.nan => 0
        
        hnew.setZero(nrows, ncols) ;
        for (int i = 0; i < n ; i ++){
            hnew(ih(i),jh(i)) = x(i) ;
        }

        /// return hnew, discharge, max_neumann_criterion
    }
    
    void ambhas_gw_2d_xy(
            const int nrows,
            const int ncols,
            double**& watershed, 
            double**& hini, 
            double**& T, 
            double**& hmin, 
            double**& par_discharge, 
            double**& net_recharge, 
            double**& Sy, 
            const double dx, 
            const double dt,
            const double alpha,
            double**& hnew,
            double**& discharge,
            double& max_neumann_criterion)
    {
        // preparation des arguments pour l'appelle de la fonction ambhas_gw_2d_xy
        MatrixXd M_watershed(nrows,ncols) ;
        MatrixXd M_hini(nrows,ncols), M_net_recharge(nrows, ncols), M_hnew(nrows, ncols), M_Sy(nrows, ncols), M_hmin(nrows, ncols), M_par_discharge(nrows, ncols), M_T(nrows, ncols) ;
        
        MatrixXd M_discharge ; 
        M_discharge.setZero(nrows, ncols) ;
        
        for (int i = 0 ; i < nrows ; i++){
            for (int j = 0 ; j <ncols ; j++){
                M_watershed(i,j) = watershed[i][j] ;
                M_hini(i,j) = hini[i][j] ;
                M_net_recharge(i,j) = net_recharge[i][j];
                M_Sy(i,j) = Sy[i][j];
                M_hmin(i,j) = hmin[i][j];
                M_par_discharge(i,j) = par_discharge[i][j];
                M_T(i,j) = T[i][j];
            }
        }
            
        ambhas_gw_2d_xy(
            nrows,
            ncols,
            M_watershed, //MatrixXd& watershed, 
            M_hini, // MatrixXd& hini, 
            M_T, //const MatrixXd& T, 
            M_hmin, //const MatrixXd&  hmin, 
            M_par_discharge, //const double par_discharge, 
            M_net_recharge, //const MatrixXd& net_recharge, 
            M_Sy, //const MatrixXd& Sy, 
            dx, //const double dx,
            dt, //const double dt,
            alpha, // const double alpha,
            M_hnew, //MatrixXd& hnew,
            M_discharge, //MatrixXd& discharge,
            max_neumann_criterion) ; //double& max_neumann_criterion)

        //maj des sorties de type double**& à partir des équivalents de type MatrixXd&
        hnew = new double*[nrows];
        discharge = new double*[nrows];
        for (int i = 0; i < nrows; ++i) {
            hnew[i] = new double[ncols];
            discharge[i] = new double[ncols];
            for (int j = 0; j < ncols; ++j) {
                hnew[i][j] = M_hnew(i,j) ;
                discharge[i][j] = M_discharge(i,j) ;
            }
        }
    }


    double min0(double x)
    { return (x > 0.) ? x : 0.; }


}
