/**
 * @file src/SticsConditionsEditor.cpp
 * @author Eric Casellas (The RECORD team -INRA)
 */

/*
 * Copyright (C) 2013 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends:  @@endtagdepends

#include <fstream>
#include <iostream> // cout
#include <sstream>

#include <vle/value/Set.hpp>
#include <vle/devs/Executive.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace Ambhas {
/**
 * @brief Modèle d'édition des conditions expérimentales pour une parcelle Stics, de type vle::devs::Executive
 *
 *
 * Ce modèle permet de modifier les valeurs de conditions expérimentales existantes
 * à partir du code PlotType de la parcelle et de la table de correspondance entre PlotType et parametres Stics
 *
 * Liste des ports
 * - Conditions Expérimentales
 *  - DbgLog (double), pour gerer le niveau de verbosité de l'affichage d'info sur la console. (valeur par défaut:0.)
 *  - PlotType (int), identifiant du type de la parcelle Stics. (valeur par défaut:0)
 *  - SticsConditionName (string), nom de la condition Stics à éditer. (valeur par défaut:cond_Stics)
 *  - ConditionsEditorSet (set<map< PlotType(int), SticsInputDir(string)>>) la condition de configuration du modèle :
 *      Chaque identifiant unique est définis par un map dans le set principal : 
 *          -PlotType (int) : l'identifiant du code de type de parcelle
 *          -SticsInputDir (string) : le parametre de Stics SticsInputDir associé au code de type de parcelle
 *          -FSol (string) : le parametre de Stics FSol associé au code de type de sol de la parcelle
 *          -FClimat (string) : le parametre de Stics FClimat associé au code de type de la parcelle
 *
 */
class SticsConditionsEditor : public vd::Executive
{
public:
    SticsConditionsEditor(const vd::ExecutiveInit& execut, const vd::InitEventList& events) :
            vd::Executive(execut, events)
    {

        DbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.0;
        DumpFileName = (events.exist("DumpFileName")) ? vv::toString(events.get("DumpFileName")) : "output_SticsConditionsEditor.vpz";
        PlotType = (events.exist("PlotType")) ? vv::toInteger(events.get("PlotType")) : 0;
        SticsConditionName = (events.exist("SticsConditionName")) ? vv::toString(events.get("SticsConditionName")) : "cond_Stics";

        if (DbgLog > 1.) {
            std::cout << "\t[SticsConditionsEditor]:\tDbgLog [" << DbgLog
                    << "]\tPlotType [" << PlotType << "]\tSticsConditionName ["
                    << SticsConditionName << "]" << std::endl;
        }

        if (events.exist("ConditionsEditorSet")) {
            const vv::Set& variables = events.getSet("ConditionsEditorSet");
            for (unsigned int index = 0; index < variables.size(); ++index) {
                const vv::Map& tab(variables.getMap(index));
                if (tab.exist("SampleParam") & tab.exist("SticsInputDir")) {
                    useStics = false;
                    ConditionsEditorSet condSet;
                    condSet.SticsInputDir = toString(tab["SticsInputDir"]);
                    condSet.SampleParam = toDouble(tab["SampleParam"]);
                    conditionsEditorMapping[toInteger(tab["PlotType"])] = condSet;
                    if (DbgLog > 1.)
                        std::cout << "\t[SticsConditionsEditor]: \tPlotType ["
                                << toInteger(tab["PlotType"]) << "] \tSticsInputDir ["
                                << conditionsEditorMapping[toInteger(tab["PlotType"])].SticsInputDir 
                                << "] \tSampleParam [" 
                                << conditionsEditorMapping[toInteger(tab["PlotType"])].SampleParam 
                                << "]"<< std::endl;
                } else if (tab.exist("SticsInputDir") & tab.exist("FClimat") & tab.exist("FSol")) {
                    useStics = true;
                    ConditionsEditorSet condSet;
                    condSet.SticsInputDir = toString(tab["SticsInputDir"]);
                    condSet.FClimat = toString(tab["FClimat"]);
                    condSet.FSol = toString(tab["FSol"]);
                    conditionsEditorMapping[toInteger(tab["PlotType"])] = condSet;
                    if (DbgLog > 1.)
                        std::cout << "\t[SticsConditionsEditor]: \tPlotType ["
                                << toInteger(tab["PlotType"]) << "] \tSticsInputDir ["
                                << conditionsEditorMapping[toInteger(tab["PlotType"])].SticsInputDir 
                                << "] \tFClimat [" 
                                << conditionsEditorMapping[toInteger(tab["PlotType"])].FClimat 
                                << "] \tFSol [" 
                                << conditionsEditorMapping[toInteger(tab["PlotType"])].FSol 
                                << "]"<< std::endl;
                } else {
                    std::stringstream ss;
                    ss << "SticsConditionsEditor - dans la condition ConditionsEditorSet le numero : " << (index + 1) << " n'a pas le bon nombre d'elements (" << tab.size() << " au lieu de 3 ou 4)" << std::endl;
                    throw vu::ModellingError(ss.str());
                }
            }
        } else {
            throw vu::ModellingError("SticsConditionsEditor - condition ConditionsEditorSet manquante");
        }





        vz::Conditions& cond_l = conditions(); // Récupération de toutes les conditions expérimentales du vpz
        if (!useStics) {
            setConditionPort(cond_l, SticsConditionName, "SticsInputDir", vv::String(conditionsEditorMapping[PlotType].SticsInputDir));
            setConditionPort(cond_l, SticsConditionName, "SampleParam", vv::Double(conditionsEditorMapping[PlotType].SampleParam));
        } else {
            setConditionPort(cond_l, SticsConditionName, "SticsInputDir", vv::String(conditionsEditorMapping[PlotType].SticsInputDir));
            setConditionPort(cond_l, SticsConditionName, "FClimat", vv::String(conditionsEditorMapping[PlotType].FClimat));
            setConditionPort(cond_l, SticsConditionName, "FSol", vv::String(conditionsEditorMapping[PlotType].FSol));
        }

        vd::Executive::createModelFromClass("SticsClass", "SticsLike");
        vd::Executive::addConnection("SticsLike", "net_recharge", coupledmodelName(), "potential_net_recharge");


        
        dumpVPZ();

    }

	virtual ~SticsConditionsEditor() {};

private:

    struct ConditionsEditorSet
    {
        std::string SticsInputDir;
        double SampleParam;
        std::string FClimat;
        std::string FSol;
        //TODO : ajouter autres param de Stics et/ou Rotator
    };

    std::map<int, ConditionsEditorSet> conditionsEditorMapping; // tableau de correspondance entre type de parcelle et les valeurs de parametres Stics associés

    double DbgLog; //niveau de verbosité
    int PlotType; //type de parcelle de la parcelle courante
    std::string SticsConditionName; //nom de la condition Stics contenant les différents paramètres
    bool useStics; // taille des conditions
    std::string DumpFileName; //nom du fichier de dump

    /*
     * @brief forçage de la valeur d'un port de condition existant
     */
    void setConditionPort(vz::Conditions& conditions,
            const std::string &conditionName, const std::string &PortName,
            const vv::Value &value)
    {
        // Accès à la condition expérimentale conditionName contenant le port PortName
        vz::Condition& cond = conditions.get(conditionName);
        cond.del(PortName);
        cond.addValueToPort(PortName, value.clone());
        if (DbgLog > 0.)
            std::cout << "\t[SticsConditionsEditor]: \tcondition ["
                    << conditionName << "] \tport [" << PortName
                    << "] \tvaleur:" << value << std::endl;
    }

    void dumpVPZ()
    {
        if (DbgLog >= 1.) {
            std::cout << "\t[SticsConditionsEditor]\tDumping file : " << DumpFileName << std::endl;
            std::ofstream file(DumpFileName.c_str());
            dump(file, "dump");
        }
    }

};
} //namespace Ambhas

DECLARE_EXECUTIVE (Ambhas::SticsConditionsEditor);

