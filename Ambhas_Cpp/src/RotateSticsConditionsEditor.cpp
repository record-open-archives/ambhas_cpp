/**
 * @file src/RotateSticsConditionsEditor.cpp
 * @author Eric Casellas (The RECORD team -INRA)
 */

/*
 * Copyright (C) 2013 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends:  @@endtagdepends

#include <fstream>
#include <iostream> // cout
#include <sstream>

#include <vle/value/Set.hpp>
#include <vle/devs/Executive.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;

namespace Ambhas {
/**
 * @brief Modèle d'édition des conditions expérimentales pour une parcelle Stics, de type vle::devs::Executive
 *
 *
 * Ce modèle permet de modifier les valeurs de conditions expérimentales existantes
 * à partir du code PlotType de la parcelle et de la table de correspondance entre PlotType et parametres Stics
 *
 * Liste des ports
 * - Conditions Expérimentales
 *  - DbgLog (double), pour gerer le niveau de verbosité de l'affichage d'info sur la console. (valeur par défaut:0.)
 *  - PlotType (int), identifiant du type de la parcelle Stics. (valeur par défaut:0)
 *  - RotateSticsConditionName (string), nom de la condition RotateStics à éditer. (valeur par défaut:cond_RotateStics)
 *  - ConditionsEditorSet (set<map< PlotType(int), SticsInputDir(string)>>) la condition de configuration du modèle :
 *      Chaque identifiant unique est définis par un map dans le set principal : 
 *          -PlotType (int) : l'identifiant du code de type de parcelle
 *          -SticsInputDir (string) : le parametre de Stics SticsInputDir associé au code de type de parcelle
 *
 */
class RotateSticsConditionsEditor : public vd::Executive
{
public:
    RotateSticsConditionsEditor(const vd::ExecutiveInit& execut, const vd::InitEventList& events) :
            vd::Executive(execut, events), conditionsEditorMapping(), DbgLog(0.), PlotType(0), RotateSticsConditionName("")
    {

        DbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.0;
        DumpFileName = (events.exist("DumpFileName")) ? vv::toString(events.get("DumpFileName")) : "output_RotateSticsConditionsEditor.vpz";
        PlotType = (events.exist("PlotType")) ? vv::toInteger(events.get("PlotType")) : 0;
        RotateSticsConditionName = (events.exist("RotateSticsConditionName")) ? vv::toString(events.get("RotateSticsConditionName")) : "cond_RotateStics";
        if (DbgLog > 1.)
            std::cout << "\t[RotateSticsConditionsEditor]:\tDbgLog [" << DbgLog
                    << "]\tPlotType [" << PlotType << "]\tRotateSticsConditionName ["
                    << RotateSticsConditionName << "]" << std::endl;

        if (events.exist("ConditionsEditorSet")) {
            const vv::Set& variables = events.getSet("ConditionsEditorSet");
            for (unsigned int index = 0; index < variables.size(); ++index) {
                const vv::Map& tab(variables.getMap(index));
                if (tab.size() == 6) {
                    ConditionsEditorSet condSet;
                    condSet.PerturbVars = const_cast < vv::Set* >(&toSetValue(tab["PerturbVars"]));
                    condSet.Rotation = const_cast < vv::Set* >(&toSetValue(tab["Rotation"]));
                    condSet.SticsCondition = const_cast<vv::String*>(&toStringValue(tab["SticsCondition"]));
                    condSet.SticsModel = const_cast < vv::String* >(&toStringValue(tab["SticsModel"]));
                    condSet.SticsObserverModel = const_cast < vv::String* >(&toStringValue(tab["SticsObserverModel"]));
                    conditionsEditorMapping[toInteger(tab["PlotType"])] = condSet;
                    if (DbgLog > 2.)
                        std::cout << "\t[RotateSticsConditionsEditor]: \tPlotType ["
                                << toInteger(tab["PlotType"]) << "]" << std::endl;
                } else {
                    std::stringstream ss;
                    ss << "RotateSticsConditionsEditor - dans la condition ConditionsEditorSet le numero : " << (index + 1) << " n'a pas le bon nombre d'elements (" << tab.size() << " au lieu de 6)" << std::endl;
                    throw vu::ModellingError(ss.str());
                }
            }
        } else {
            throw vu::ModellingError("RotateSticsConditionsEditor - condition ConditionsEditorSet manquante");
        }

        // Récupération de toutes les conditions expérimentales du vpz
        vz::Conditions& cond_l = conditions();

        if (DbgLog > 1.)
            std::cout << "\t[RotateSticsConditionsEditor]:\tEditing Rotator parameters..." << std::endl;
        setConditionPort(cond_l, RotateSticsConditionName, "PerturbVars", *conditionsEditorMapping[PlotType].PerturbVars);
        setConditionPort(cond_l, RotateSticsConditionName, "Rotation", *conditionsEditorMapping[PlotType].Rotation);
        setConditionPort(cond_l, RotateSticsConditionName, "SticsCondition", *conditionsEditorMapping[PlotType].SticsCondition);
        setConditionPort(cond_l, RotateSticsConditionName, "SticsModel", *conditionsEditorMapping[PlotType].SticsModel);
        setConditionPort(cond_l, RotateSticsConditionName, "SticsObserverModel", *conditionsEditorMapping[PlotType].SticsObserverModel);

        if (DbgLog > 1.)
            std::cout << "\t[RotateSticsConditionsEditor]:\tAdding Rotator model..." << std::endl;
        vd::Executive::createModelFromClass("RotatorClass", "RotateStics");
        
        const vz::ConnectionList& list = getModel().getParent()->getInputPortList();
        for (vz::ConnectionList::const_iterator it = list.begin(); it != list.end(); ++it) {
            if (DbgLog > 1.)
                std::cout << "\t[RotateSticsConditionsEditor]:\tConnecting Rotator input: " << it->first << std::endl;

            addConnection(coupledmodelName(), it->first, "RotateStics" , it->first);
        }

        
        dumpVPZ();

    }

	virtual ~RotateSticsConditionsEditor() {};

private:

    struct ConditionsEditorSet
    {
        vv::Set* PerturbVars;
        vv::Set* Rotation;
        vv::String* SticsCondition;
        vv::String* SticsModel;
        vv::String* SticsObserverModel;
    };

    std::map<int, ConditionsEditorSet> conditionsEditorMapping; // tableau de correspondance entre type de parcelle et les valeurs de parametres Stics associés

    double DbgLog; //niveau de verbosité
    int PlotType; //type de parcelle de la parcelle courante
    std::string RotateSticsConditionName; //nom de la condition Stics contenant les différents paramètres
    std::string DumpFileName; //nom du fichier de dump

    /*
     * @brief forçage de la valeur d'un port de condition existant
     */
    void setConditionPort(vz::Conditions& conditions,
            const std::string &conditionName, const std::string &PortName,
            const vv::Value &value)
    {
        // Accès à la condition expérimentale conditionName contenant le port PortName
        vz::Condition& cond = conditions.get(conditionName);
        cond.del(PortName);
        cond.addValueToPort(PortName, value.clone());
        if (DbgLog > 0.)
            std::cout << "\t[RotateSticsConditionsEditor]: \tcondition ["
                    << conditionName << "] \tport [" << PortName
                    << "] \tvaleur:" << value << std::endl;
    }

    void dumpVPZ()
    {
        if (DbgLog >= 1.) {
            std::cout << "\t[RotateSticsConditionsEditor]\tDumping file : " << DumpFileName << std::endl;
            std::ofstream file(DumpFileName.c_str());
            dump(file, "dump");
        }
    }


};
} //namespace Ambhas

DECLARE_EXECUTIVE (Ambhas::RotateSticsConditionsEditor);

