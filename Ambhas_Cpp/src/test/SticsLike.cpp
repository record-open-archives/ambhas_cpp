/**
 * @file src/test/SticsLike.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2013 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <iomanip>      // std::setprecision
#include <iostream>     // std::cout


namespace vd = vle::devs;
namespace vv = vle::value;

namespace Ambhas {
namespace test {
using namespace vle::discrete_time;

class SticsLike : public DiscreteTimeDyn
{
public:
    SticsLike(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events)
    {
        DbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.;
        if (DbgLog > 0.)
            std::cout << "\t[SticsLike]\tStarting constructor for instance:" << getModel().getCompleteName() << std::endl;

        net_recharge.init(this,"net_recharge", events);

        useRand = (events.exist("useRand")) ? vv::toBoolean(events.get("useRand")) : true;
        SampleParam = (events.exist("SampleParam")) ? vv::toDouble(events.get("SampleParam")) : 0.;
        SticsInputDir = (events.exist("SticsInputDir")) ? vv::toString(events.get("SticsInputDir")) : "";
        id = (events.exist("id")) ? vv::toInteger(events.get("id")) : 0;

        if (DbgLog > 1.) {
            std::cout << "\t[SticsLike]\t\tDbgLog:" << DbgLog << std::endl;
            std::cout << "\t[SticsLike]\t\tSampleParam:" << SampleParam << std::endl;
            std::cout << "\t[SticsLike]\t\tSticsInputDir:" << SticsInputDir << std::endl;
            std::cout << "\t[SticsLike]\t\tid:" << id << std::endl;
        }

        if (DbgLog > 0.)
            std::cout << "\t[SticsLike]\tEnding constructor for instance:"
                    << getModel().getCompleteName() << std::endl;
    }

    virtual ~SticsLike() {};

    virtual void compute(const vd::Time& time)
    {
        if (DbgLog > 1.)
            std::cout << std::setprecision(10) << time
                    << "\t[SticsLike]\tStarting compute for instance:"
                    << getModel().getCompleteName() << std::endl;

//        net_recharge = SampleParam * (0.5 * ((double) rand() / RAND_MAX + 1) - 0.25) / 1000;
        if (useRand) {
                net_recharge =  ((double)rand() / RAND_MAX )*0.04-0.02 ;
        } else {
                 net_recharge = SampleParam;
        }
        if (DbgLog > 1.)
            std::cout << std::setprecision(10) << time
                    << "\t[SticsLike]\t\tnet_recharge:" << net_recharge()
                    << std::endl;

        if (DbgLog > 1.)
            std::cout << std::setprecision(10) << time
                    << "\t[SticsLike]\tEnding compute for instance:"
                    << getModel().getCompleteName() << std::endl;
    }

    virtual void initValue(const vd::Time& time)
    {
        if (DbgLog > 0.)
            std::cout << std::setprecision(10) << time
                    << "\t[SticsLike]\tStarting initValue for instance:"
                    << getModel().getCompleteName() << std::endl;
        net_recharge = 0.;
        if (DbgLog > 0.)
            std::cout << std::setprecision(10) << time
                    << "\t[SticsLike]\tEnding initValue for instance:"
                    << getModel().getCompleteName() << std::endl;
    }

private:
    Var net_recharge;

    double SampleParam;
    double DbgLog;
    int id;
    std::string SticsInputDir;
    bool useRand;

};
}
} //namespaces
DECLARE_DYNAMICS (Ambhas::test::SticsLike); // balise specifique VLE


