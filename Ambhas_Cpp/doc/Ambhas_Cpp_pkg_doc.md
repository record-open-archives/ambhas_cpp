
# Package Ambhas_Cpp for vle-2.0.2

The **Ambhas_Cpp** package provide a main model of interest called **Ambhas_Cpp**, 3 other models can also be usefull **GenGRDcan_cpp**, **RotateSticsConditionsEditor** and **SticsConditionsEditor** (an additionnal model is also in the package to provide some sample simulators, but it will not be documented here). 
Sample input files for those sample simulator are also provided in the */data* folder.

The **Ambhas_Cpp** is a distributed groundwater model for simulating daily evolution of the water table level.
The **GenGRDcan_cpp** model purpose is to help setting up the simulator automatically.
The **RotateSticsConditionsEditor** and **SticsConditionsEditor** models purpose are to help setting up a class of model's bundle of experimental conditions from a single input key value.

# Table of Contents
1. [Package dependencies](#p1)
2. [Atomic model Ambhas_Cpp](#p2)
3. [Atomic model GenGRDcan_cpp](#p3)
4. [Atomic model RotateSticsConditionsEditor](#p4)
5. [Atomic model SticsConditionsEditor](#p5)

---

## Package build dependencies <a name="p1"></a>

List of required external packages (with link to distribution when available).

* [vle.discrete-time](http://www.vle-project.org/pub/2.0/vle.discrete-time.tar.bz2)
* [ext.Eigen](http://recordb.toulouse.inra.fr/distributions/2.0/ext.Eigen.tar.bz2)
* [vle.reader](https://www.vle-project.org/pub/2.0/vle.reader.tar.bz2)
* [vle.tester](http://www.vle-project.org/pub/2.0/vle.tester.tar.bz2)

--- 

## Atomic model Ambhas_Cpp <a name="p2"></a>

The **Ambhas_Cpp** is a distributed groundwater model. It simulates the daily groundwater level variations. The model is based on McDonald and Harbaugh equations (1988).  
More precisely, for an unconfined aquifer in 2D, the groundwater flow is calculated from the Equation 1 (McDonald & Harbaugh, 1988)  
This version is a C++ translation of a Python source code from Sat Tomer [source](https://pypi.org/project/ambhas/)  

The **Ambhas_Cpp** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Ambhas_panel'>Show/Hide Model Details</button>
<div id="Ambhas_panel" class="collapse">

### Configuring a Ambhas_Cpp Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : Ambhas_Cpp
* **name** : Ambhas_Cpp

#### Parameters settings <a name="p2.1.2"></a>

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

List and informations about all possible parameters (name, type, default value, ismandatory, description).

|Parameter name | Type | Default value | Is mandatory ? | Description&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;|
| -- | :--: | :--: | :--: | -- |
| DbgLog | double | 0 | [] | Verbosity level for debug info (console outputs)|
| n | integer | 0 | [] | Number of active plot in watershed from GenGRDcan to compare to local active plots read in InputGrid file|
| PkgName | string | Ambhas_Cpp | [] | VLE package name containing the GIS files|
| InputGrid | string | watershed.grd | [] | File name of the GIS file (use ASCII grid format)|
| InputGrid_hini | string |  | [] | File name of the GIS file (use ASCII grid format) with initial groundwater level for all watershed pixels (m) |
| hini | double | 600 | [] | Initial groundwater level for all watershed pixels (m), used if and only if *InputGrid_hini* is not provided |
| DemGrid | string | - | [] | Optional file name of the GIS file (use ASCII grid format) for soil surface elevation in m |
| dem | double | Inf | [] | soil surface elevation (m), used if and only if *DemGrid* is not provided |
| SoilThickness | double | 0 | [] | distance between soil surface and GW max elevation in m |
| NamingMode | string | x_y | [] | Naming convention code (see details for more info)|
| dt | integer | 1 | [] | time step (an internal one for H(t) computation, not related to the *time_step* parameter from `DiscreteTimeDyn`) (s) |
| InputGrid_hmin | string |  | [] | File name of the GIS file (use ASCII grid format) with Groundwater level corresponding to 0 discharge for all watershed pixels (m) |
| hmin | double | 580 | [] | Groundwater level corresponding to 0 discharge (m), used if and only if *InputGrid_hmin* is not provided |
| InputGrid_Sy | string |  | [] | File name of the GIS file (use ASCII grid format) with specific yield for all watershed pixels (m) |
| Sy | double | 0.005 | [] | Specific yield (-), used if and only if *InputGrid_Sy* is not provided |
| InputGrid_par_discharge | string |  | [] | File name of the GIS file (use ASCII grid format) with parameter controlling the discharge for all watershed pixels (-) |
| par_discharge | double | 0.999 | [] | Parameter controlling the discharge (-), used if and only if *InputGrid_par_discharge* is not provided |
| InputGrid_T | string |  | [] | File name of the GIS file (use ASCII grid format) with transmissivity for all watershed pixels (mm.s<sup>-1</sup>) |
| T | double | 20 | [] | Transmissivity (mm.s<sup>-1</sup>), used if and only if *InputGrid_T* is not provided |
| RechargeConversionFactor | double | 1 | [] | Conversion factor for *potential_net_recharge* inputs (user defined) |


#### Input settings <a name="p2.1.3"></a>

See additionnal details on Naming convention. 

* **potential_net_recharge_i :** Daily potential water net flow in the groundwater from above for the i<sup>th</sup> pixel (m<sup>3</sup>.day<sup>-1</sup>) (double) (sync)  

#### Output settings <a name="p2.1.4"></a>

See additionnal details on Naming convention. 

* **actual_net_recharge_i :** Daily actual water net flow in the groundwater from above for the i<sup>th</sup> pixel(m.day<sup>-1</sup>) (a.k.a. m<sup>3</sup>.m<sup>-2</sup>.day<sup>-1</sup>) (volume of water per m2 of GW pixel surface) (double) 
* **H_i :** Hydraulic head, groundwater level above the sea for the i<sup>th</sup> pixel(m) (double) 
* **GWVol_i :** Groundwater volume for the i<sup>th</sup> pixel(m<sup>3</sup>) (double)   
* **discharge_i :** daily discharge for the i<sup>th</sup> pixel () (double)   
* **GW_Overland_Flow_i :** daily groundwater overflow for the i<sup>th</sup> pixel (m<sup>3</sup>) (double)   

#### Observation settings <a name="p2.1.5"></a>

None

#### Available configurations <a name="p2.1.6"></a>

None

### Details <a name="p2.2"></a>

#### State variables equations <a name="p2.2.1"></a>

> GWpixelArea (m<sup>2</sup>) : surface of the groundwater pixel is computed from cellsize info read in input GIS file is identical for all pixels (rasters)

> $actual\_net\_recharge\_i(t) = max\{RechargeConversionFactor * \frac{potential\_net\_recharge\_i(t)}{GWpixelArea} ; -H\_i(t-1)*Sy\_i\}$  

> $discharge\_i(t) = (1-par\_discharge\_i) * (H\_i(t-1)-hmin\_i) * Sy\_i$

> $$H\_i(t) = \min\left(
  \begin{array}{@{}ll@{}}
    hnew\_i \\
    DEM\_i - SoilThickness
  \end{array}\right)$$
  with 
     $hnew\_i = H\_i(t-1) + \frac{actual\_net\_recharge\_i(t) - discharge\_i(t)}{Sy\_i}$ + ... net lateral flows from other pixels (see McDonald & Harbaugh, 1988)

> $GW\_Overland\_Flow\_i(t) = (hnew\_i - H\_i(t)) * Sy\_i * GWpixelArea$


> $GWVol\_i(t) = max\{0. ; (H\_i(t) - hmin\_i) * Sy\_i * GWpixelArea)$ 


#### Naming conventions <a name="p2.2.2"></a>
if NamingMode == "x_y" :
        all variables (inputs & outputs) are suffixed with a _x_y identifier where x and y are respectively the row and column indexes of a given pixel
else :
        all variables are suffixed with a _id identifier (loop over rows then columns) (for example with a x=9;y=10 grid id=1 is the first row/column, id=2 the second column of the first row, id=9 is the last column of the first row, id=10 is the first column of the second row, and so on...)


#### References 
McDonald, M., & Harbaugh, A. (1988). A modular three-dimensional finite-difference groundwater flow model. Washington: Denver.  
Park, E. and Parker, J. (2008). A simple model for water table fluctuations in response to precipitation. Journal of Hydrology, 356:344 – 349.  
Simon, D. (2006). Optimal State Estimation: Kalman, H-infinity, and Nonlinear Approaches. John Wiley and Sons.  

</div>


## Atomic model GenGRDcan_cpp <a name="p3"></a>

>todo

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#GenGRDcan_panel'>Show/Hide Model Details</button>
<div id="GenGRDcan_panel" class="collapse">

### Configuring a YY Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : XX
* **name** : YY

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

for example :  
See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim or generic_with_header) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

#### Available configurations <a name="p2.1.6"></a>

List and information about all configuration metadata files

### Details <a name="p2.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

</div>

## Atomic model RotateSticsConditionsEditor <a name="p4"></a>

>todo

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#RotateSticsConditionsEditor_panel'>Show/Hide Model Details</button>
<div id="RotateSticsConditionsEditor_panel" class="collapse">

### Configuring a YY Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : XX
* **name** : YY

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

for example :  
See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim or generic_with_header) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

#### Available configurations <a name="p2.1.6"></a>

List and information about all configuration metadata files

### Details <a name="p2.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

</div>

## Atomic model SticsConditionsEditor <a name="p5"></a>

>todo

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#SticsConditionsEditor_panel'>Show/Hide Model Details</button>
<div id="SticsConditionsEditor_panel" class="collapse">

### Configuring a YY Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : XX
* **name** : YY

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

for example :  
See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim or generic_with_header) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

#### Available configurations <a name="p2.1.6"></a>

List and information about all configuration metadata files

### Details <a name="p2.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

</div>


